//
//  CellTitle.swift
//  BongDa
//
//  Created by APPLE on 4/17/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class CellTitle:UITableViewCell {
    @IBOutlet weak var cell_View:UIView!
    
    override func layoutSubviews() {
        cell_View.layer.borderColor = UIColor.groupTableViewBackgroundColor().CGColor
        cell_View.layer.borderWidth = 2
    }
}
