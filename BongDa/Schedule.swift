//
//  Schedule.swift
//  BongDa
//
//  Created by APPLE on 4/18/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class Schedule {
    var teamGroup:String?
    var matchs:[Matchs]?
    
    init(dic_Schedule:NSDictionary){
        
        if let _teamGroup = dic_Schedule["teamGroup"] as? String {
            teamGroup = _teamGroup
        }

        if let _matchs = dic_Schedule["matchs"] as? [NSDictionary] {
            
            var arrMatch = [Matchs]()
            for mat in _matchs {
                arrMatch.append(Matchs(dic_Matchs: mat))
            }
            matchs = arrMatch
            print(matchs!.count)
            
        }
    }
}

class Matchs{
    var league:String?
    var teams:Teams?
    var time:String?
    var date:String?
    
    init (dic_Matchs:NSDictionary) {
        if let _league = dic_Matchs["league"] as? String {
            league = _league
        }
        if let _time = dic_Matchs["time"] as? String {
            time = _time
        }
        if let _date = dic_Matchs["date"] as? String {
            date = _date
        }
        if let _team = dic_Matchs["teams"] as? NSDictionary {
            teams = Teams(dic_Teams: _team)
        }
    }
}


class Teams{
    var logo1:NSURL?
    var logo2:NSURL?
    var name1:String?
    var name2:String?
    
    init (dic_Teams: NSDictionary){
        if let _logo1 = dic_Teams["logo1"] as? String {
            logo1 = NSURL(string: _logo1)
        }
        if let _logo2 = dic_Teams["logo2"] as? String {
            logo2 = NSURL(string: _logo2)
        }
        if let _name1 = dic_Teams["name1"] as? String {
            name1 = _name1
        }
        if let _name2 = dic_Teams["name2"] as? String {
            name2 = _name2
        }
    }
}