//
//  BDHighlightCell.swift
//  BongDa
//
//  Created by APPLE on 4/19/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class BDHighlightCell: UITableViewCell {
    @IBOutlet weak var cell_Thumbnail:UIImageView!
    @IBOutlet weak var cell_Title:UILabel!
    @IBOutlet weak var cell_League:UILabel!
}
