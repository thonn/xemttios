//
//  AddCoin.swift
//  BongDa
//
//  Created by APPLE on 4/30/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import Foundation

class AddCoin {
    var sms_info:SMS_Info?
    var payments:[Payments]?
    init (dic_AddCoin:NSDictionary){
        if let _sms_info = dic_AddCoin["sms_info"] as? NSDictionary {
            sms_info = SMS_Info(dic_SMS_Info: _sms_info)
        }
        if let _payments = dic_AddCoin["payments"] as? [NSDictionary] {
            var arrPay = [Payments]()
            for pay in _payments {
                arrPay.append(Payments(dic_Payments: pay))
            }
            payments = arrPay
        }
    }
}

class SMS_Info {
    var gateway_number:String?
    var viettel:String?
    var mobifone:String?
    var vinaphone:String?
    
    init (dic_SMS_Info: NSDictionary){
        if let _gateway_number = dic_SMS_Info["gateway_number"] as? String {
            gateway_number = _gateway_number
        }
        if let _viettel = dic_SMS_Info["viettel"] as? String {
            viettel = _viettel
        }
        if let _mobifone = dic_SMS_Info["mobifone"] as? String {
            mobifone = _mobifone
        }
        if let _vinaphone = dic_SMS_Info["vinaphone"] as? String {
            vinaphone = _vinaphone
        }
    }
}

class Payments {
    var type:Int?
    var value:String?
    var cost:String?
    
    init (dic_Payments: NSDictionary){
        if let _type = dic_Payments["type"] as? Int {
            type = _type
        }
        if let _value = dic_Payments["value"] as? String {
            value = _value
        }
        if let _cost = dic_Payments["cost"] as? String {
            cost = _cost
        }
    }
}

