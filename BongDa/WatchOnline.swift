//
//  WatchOnline.swift
//  BongDa
//
//  Created by APPLE on 4/17/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class WatchOnline {
    var id:Int?
    var title:String?
    var team1:String?
    var team2:String?
    var live_logo_a:UIImage?
    var live_logo_b:UIImage?
    var date:String?
    var league:String?
    var time:String?
    var cost:Bool?
    
    init (dic_WatchOnline:NSDictionary){
        
        if let _id = dic_WatchOnline["id"] as? Int {
            id = _id
        }
        if let _title = dic_WatchOnline["title"] as? String{
            title = _title
        }
        if let _team1 = dic_WatchOnline["team1"] as? String {
            team1 = _team1
        }
        if let _team2 = dic_WatchOnline["team2"] as? String {
            team2 = _team2
        }
        if let _live_logo_a = dic_WatchOnline["live_logo_a"] as? String {
            let data = NSData(contentsOfURL: NSURL(string: _live_logo_a)!)
            live_logo_a = UIImage(data: data!)
        }
        if let _live_logo_b = dic_WatchOnline["live_logo_b"] as? String {
            let data = NSData(contentsOfURL: NSURL(string: _live_logo_b)!)
            live_logo_b = UIImage(data: data!)
        }
        if let _date = dic_WatchOnline["date"] as? String {
            date = _date
        }
        if let _league = dic_WatchOnline["league"] as? String {
            league = _league
        }
        if let _time = dic_WatchOnline["time"] as? String {
            time = _time
        }
        if let _cost = dic_WatchOnline["cost"] as? Bool {
            cost = _cost
        }
        
    }
}
