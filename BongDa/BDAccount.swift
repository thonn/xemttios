    //
//  BDAccount.swift
//  BongDa
//
//  Created by APPLE on 4/20/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class BDAccount:UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var btnLogin: FBSDKLoginButton!
    @IBOutlet weak var btnLogout: FBSDKLoginButton!
    
    
    @IBOutlet weak var myAvatar: UIImageView!
    @IBOutlet weak var myName: UILabel!
    @IBOutlet weak var myCoin: UILabel!
    @IBOutlet weak var myBigView: UIView!
    @IBOutlet weak var myViewHistory: UIView!
    
    
    @IBOutlet weak var myViewHeight: NSLayoutConstraint!
    
    
    var account:Account?
    var history = [History]()
    var accessToken:String!
    var fid:String!
    
    override func viewDidLoad() {
        setupData()
        setupView()

//        btnLogout.layer.frame.size.height = 200
        btnLogout.frame.size.height = 200
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
        }
    }

    
    func setupData(){
        btnLogin.delegate = self
        btnLogout.delegate = self
        if FBSDKAccessToken.currentAccessToken() != nil {
            parseData(FBSDKAccessToken.currentAccessToken().tokenString)
            btnLogin.hidden = true
            myBigView.hidden = false
        }else {
            myBigView.hidden = true
            btnLogin.hidden = false
        }
    }
    
    func setupView(){
        Utility.setTitleNavi(self)
        myAvatar.layer.cornerRadius = myAvatar.frame.size.width / 2
        let gesture = UITapGestureRecognizer(target: self, action: "showZopimAction:")
        let fab = KCFloatingActionButton()
        self.view.addSubview(fab)
        fab.addGestureRecognizer(gesture)
        
        
        
    }
    
    func showZopimAction(sender:UITapGestureRecognizer){
        // do other task
        self.performSegueWithIdentifier("viewZopim", sender: nil)
    }

    
    func parseData(accessToken: String){
        
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_LOGIN)\(accessToken)"
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                print(responseObject)
                
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    if let dic_user = dataReponse["user"] as? NSDictionary {
                        NSUserDefaults.standardUserDefaults().setObject(dic_user["token"], forKey: "access")
                        NSUserDefaults.standardUserDefaults().setObject(dic_user["facebook_id"], forKey: "fbid")
                        
                        self.account = Account(dic_Account: dic_user)
                        self.setDataForAccount()
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func parseDataProfile(){
        self.history.removeAll()
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_GETPROFILE)&accessToken=\(accessToken)&fid=\(fid!)"
        print(URLString )
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                print(responseObject)
                
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    if let dic_History = dataReponse["history"] as? [NSDictionary] {
                        print(dic_History)
                        
                        for dic_His in dic_History {
                            self.history.append(History(dic_History: dic_His))
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.addHistory()
                            })
                        }
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func addHistory(){
        print(history.count)
        
        for sub in myViewHistory.subviews {
            sub.removeFromSuperview()
        }
        let theWidth = view.frame.size.width
        let theX:CGFloat = 8
        var theY:CGFloat = 8
        myViewHeight.constant = 10
        for his in history {
            let contentView = UIView(frame: CGRect(x: theX, y: theY, width: theWidth - 32, height: 44))
            myViewHistory.addSubview(contentView)
            setContentHistory(contentView, his: his)
            
            
            
            
            
            myViewHeight.constant += 48
            self.view.layoutIfNeeded()
            theY = theY + 48
        }
    }
    
    func setContentHistory(contentView: UIView, his: History){
        let theWidth = view.frame.size.width
        //set img for history
        let imgHis:UIImageView = UIImageView(frame: CGRect(x: 0, y: 8, width: 36, height: 36))
        contentView.addSubview(imgHis)
        if his.coins?.characters.first == "+" {
            imgHis.image = UIImage(named: "cong")
        }else {
            imgHis.image = UIImage(named: "tru")
        }
        
        
        //set title for history
        let lblTitle:UILabel = UILabel(frame: CGRect(x: 48, y: 0, width: theWidth/2 - 30, height: 44))
        lblTitle.text = his.action
//        lblTitle.backgroundColor = UIColor.redColor()
        lblTitle.font = UIFont.systemFontOfSize(13)
        contentView.addSubview(lblTitle)
        
        //set date history
        let lblDate:UILabel = UILabel(frame: CGRect(x: 48 + theWidth/2 - 30 + 4, y: 0, width: contentView.frame.size.width - (lblTitle.frame.origin.x + (theWidth/2 - 30)) - 4, height: 44))
        lblDate.text = his.time
        lblDate.font = UIFont.systemFontOfSize(13)
//        lblDate.backgroundColor = UIColor.redColor()
        contentView.addSubview(lblDate)
    }
    
    func setDataForAccount(){
        myCoin.text! = String(account!.coins!) + " coins"
        myAvatar.image = UIImage(named: "0")
        myName.text = account?.name
        
        let requestOperation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: (account?.picture)!))
        
        requestOperation.responseSerializer = AFImageResponseSerializer()
        requestOperation.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
            print(responseObject)
            
            self.myAvatar.image = responseObject as? UIImage
            
            }) { (operation, error) -> Void in
                print(error)
        }
        requestOperation.start()
        
        if NSUserDefaults.standardUserDefaults().objectForKey("access") != nil{
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
            parseDataProfile()
        }
        
    }
    
    func parseDataShareFB(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_SHAREFB)&accessToken=\(accessToken)&fid=\(fid)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let _data = responseObject["data"] as? NSDictionary {
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    @IBAction func actionMuaThang(sender: AnyObject) {
        print("mua tk thang")
    }
    
    @IBAction func btnShare(sender: UIBarButtonItem) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            parseDataShareFB()
            Utility.shareFB(self)
            
        }else {
            Utility.showAlert("Bạn chưa đăng nhập, vui lòng đăng nhập")
        }
    }
    
    @IBAction func actionNap(sender: AnyObject) {
        print("Nap them coin")
    }

    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        parseData(FBSDKAccessToken.currentAccessToken().tokenString)
        btnLogin.hidden = true
        myBigView.hidden = false
        
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("da logout")
        
        myBigView.hidden = true
        btnLogin.hidden = false
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "access")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "fbid")
        

    }
}
