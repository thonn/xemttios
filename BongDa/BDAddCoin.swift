//
//  BDAddCoin.swift
//  BongDa
//
//  Created by APPLE on 4/30/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit
import MessageUI
import CoreTelephony

class BDAddCoin:UIViewController {
    
    
    
    
    
    
    //-----------------------------Outlet vs Varible----------------------------------//
    
    //Label Coin vs Day
    @IBOutlet weak var lblNgay1: UILabel!
    @IBOutlet weak var lblCoin1: UILabel!
    
    @IBOutlet weak var lblNgay2: UILabel!
    @IBOutlet weak var lblCoin2: UILabel!
    @IBOutlet weak var imgCoin2: UIImageView!
    @IBOutlet weak var lblNgay3: UILabel!
    @IBOutlet weak var lblCoin3: UILabel!
    @IBOutlet weak var imgCoin3: UIImageView!
    @IBOutlet weak var lblNgay4: UILabel!
    @IBOutlet weak var lblCoin4: UILabel!
    @IBOutlet weak var imgCoin4: UIImageView!
    @IBOutlet weak var lblNgay5: UILabel!
    @IBOutlet weak var lblCoin5: UILabel!
    @IBOutlet weak var imgCoin5: UIImageView!
    
    //View Coin vs Day
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var viewParent: UIView!
    
    //Add Coin Info
    @IBOutlet weak var txtMaThe:UITextField!
    @IBOutlet weak var txtSeri:UITextField!
    @IBOutlet weak var lblChonMang:UILabel!
    @IBOutlet weak var imgChonMang:UIImageView!
    
    //PickerView
    @IBOutlet weak var myPicker: UIPickerView!
    
    //Varible
    var _addCoin:AddCoin?
    var arrNhaMang:[String] = ["mobifone", "viettel", "vinaphone"]
    var accessToken:String!
    var fid:String!
    
    //-----------------------------ViewDidLoad----------------------------------//
    override func viewDidLoad() {
        setupViewLayout()
        setupData()
    }
    
    
    //MARK: SETUP DATA VS SETUP VIEW
    func setupViewLayout(){
        tabBarController?.tabBar.hidden = true
        
        viewParent.hidden = true
        viewParent.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
        
        lblChonMang.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "actionChonMang:"))
        imgChonMang.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "actionChonMang:"))
        
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.hidden = true
        myPicker.layer.borderColor = UIColor.groupTableViewBackgroundColor().CGColor
        myPicker.layer.borderWidth = 1
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Nạp coin", style: UIBarButtonItemStyle.Plain, target: self, action: "actionBack")
    }
    
    func setupData(){
        if NSUserDefaults.standardUserDefaults().objectForKey("access") != nil{
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
        }
        
        parseData()
    }
    
    func parseData(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = URL_ADDCOIN
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    self._addCoin = AddCoin(dic_AddCoin: dataReponse)
                    self.setupView()
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func parseDataAddCoin(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if lblChonMang.text == "Chọn mạng" {
            lblChonMang.text = "mobifone"
        }
        
        let URLString:String = "\(URL_NAPCARD)&type=\(lblChonMang.text!)&pin=\(txtMaThe.text!)&serial=\(txtSeri.text!)&accessToken=\(accessToken)&fid=\(fbid!)"
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let _data = responseObject["data"] as? NSDictionary {
                    
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func setupView(){
        if self._addCoin?.payments?[0].type == 0 {
            lblNgay1.text = "\(self._addCoin!.payments![0].value!)"
            lblCoin1.text = self._addCoin?.payments?[0].cost
            view1.tag = 1
            view1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "selectCoin:"))
        }
        if self._addCoin?.payments?[1].type == 1 {
            lblNgay2.text = "\(self._addCoin!.payments![1].value!)"
            lblCoin2.text = self._addCoin?.payments?[1].cost
            imgCoin2.image = UIImage(named: "coin-s")
            view2.tag = 2
            view2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "selectCoin:"))
        }
        if self._addCoin?.payments?[2].type == 2 {
            lblNgay3.text = "\(self._addCoin!.payments![2].value!)"
            lblCoin3.text = self._addCoin?.payments?[2].cost
            imgCoin3.image = UIImage(named: "coin-m")
            view3.tag = 3
            view3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "selectCoin:"))
        }
        if self._addCoin?.payments?[3].type == 2 {
            lblNgay4.text = "\(self._addCoin!.payments![3].value!)"
            lblCoin4.text = self._addCoin?.payments?[3].cost
            imgCoin4.image = UIImage(named: "coin-l")
            view4.tag = 4
            view4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "selectCoin:"))
        }
        if self._addCoin?.payments?[4].type == 2 {
            lblNgay5.text = "\(self._addCoin!.payments![4].value!)"
            lblCoin5.text = self._addCoin?.payments?[4].cost
            imgCoin5.image = UIImage(named: "coin-xl")
            view5.tag = 5
            view5.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "selectCoin:"))
        }
    }
    
    //MARK: ACTION
    @IBAction func actionNapThe(butNap:UIButton) {
        viewParent.hidden = true
        parseDataAddCoin()
    }
    
    @IBAction func actionHuy(butHuy:UIButton){
        viewParent.hidden = true
    }
    
    func actionChonMang(recognizer: UITapGestureRecognizer){
        myPicker.hidden = false
    }
    
    func actionBack(){
        tabBarController?.tabBar.hidden = false
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func buyAccountMonth(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_ACCOUNTMONT)&accessToken=\(accessToken)&fid=\(fbid!)"
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{                
                if let _data = responseObject["data"] as? NSDictionary {
                    
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    //MARK: SELECT VS ADD COIN CATE
    func selectCoin(recognizer: UITapGestureRecognizer){
        if accessToken != nil {
            switch (recognizer.view!.tag) {
            case 1:
                buyAccountMonth()
            case 2 :
                
                let networkInfo = CTTelephonyNetworkInfo()
                let carrier = networkInfo.subscriberCellularProvider

                if carrier != nil {
                    let carrierNetworkCode = carrier!.mobileNetworkCode!
                    let carrierCountryCode = carrier!.mobileCountryCode!
                    if carrierCountryCode == "452" {
                        switch carrierNetworkCode {
                        case "01":
                            setupMessage("MW NG7 NAP10 \(fid)")
                        case "02":
                            setupMessage("MW NG7 NAP10 \(fid)")
                        case "04":
                            setupMessage("MW 10000 NG7 NAP \(fid)")
                        default:
                            return
                        }
//                        if carrierNetworkCode == "01" {
//                            setupMessage("MW NG7 NAP10")
//                        }else if carrierNetworkCode == "02" {
//                            setupMessage("MW NG7 NAP10")
//                        }else if carrierNetworkCode == "04" {
//                            setupMessage("MW 10000 NG7 NAP")
//                        }
                    }
                }else {
                    Utility.showAlert("Thiết bị của bạn không có sim")
                }
                
            case 3..<6:
                viewParent.hidden = false
            default :
                return
            }
        }else {
            Utility.showAlert("Bạn chưa đăng nhập tài khoản")
            
        }
    }
    
    
    //MARK: SETUP MESSAGE
    func setupMessage (messageBody:String){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = messageBody
            controller.recipients = ["9029"]
            controller.messageComposeDelegate = self
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
}

extension BDAddCoin:MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        
        switch (result.rawValue) {
        case MessageComposeResultCancelled.rawValue:
            print("Message was cancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultFailed.rawValue:
            print("Message failed")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultSent.rawValue:
            print("Message was sent")
            self.dismissViewControllerAnimated(true, completion: nil)
        default:
            break;
        }
    }
}

extension BDAddCoin:UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrNhaMang.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrNhaMang[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lblChonMang.text = arrNhaMang[row]
        myPicker.hidden = true
    }
}