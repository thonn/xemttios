//
//  BDHighlight.swift
//  BongDa
//
//  Created by APPLE on 4/19/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class BDHighlight: UIViewController {
    @IBOutlet weak var tableHighlight:UITableView!
    
    var _Highlight = [Highlight]()
    var arrIsShow = [[Bool]]()
    
    var page:Int = 0
    var rowShow = 0
    var isLoad = true
    var selectedIndexPaths = NSMutableSet()

    var id:Int?
    var accessToken:String!
    var fid:String!
    private var firstAppear = true
    
    override func viewDidLoad() {
        setupData()
        setupView()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
        }
    }
    
    //MARK: SetupView vs Data
    func setupView(){
        Utility.setTitleNavi(self)
        let gesture = UITapGestureRecognizer(target: self, action: "showZopimAction:")
        let fab = KCFloatingActionButton()
        self.view.addSubview(fab)
        fab.addGestureRecognizer(gesture)
        
        
        
    }
    
    func showZopimAction(sender:UITapGestureRecognizer){
        // do other task
        self.performSegueWithIdentifier("viewZopim", sender: nil)
    }

    
    func setupData(){
        parseData()
        tableHighlight.delegate = self
        tableHighlight.dataSource = self
        
        
    }
    
    //MARK: Parse Data
    func parseData(){
        
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_HIGHLIGHT)\(page)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    let queue = dispatch_queue_create("lelam", DISPATCH_QUEUE_SERIAL)
                    dispatch_sync(queue, { () -> Void in
                        self._Highlight.append(Highlight(dic_Highlight: dataReponse))
                        
                        //                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.tableHighlight.reloadData()
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            print("hello")
                            self.isLoad = true
                            if self.view.frame.size.height - self.tableHighlight.contentSize.height > 44 {
                                self.page += self.page + 1
                                self.parseData()
                            }
                        })
                        //                        })
                    })
                    
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    
    func configure(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        if selectedIndexPaths.containsObject(indexPath) {
            let requestOperation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _Highlight[indexPath.section].matchs![indexPath.row].thumbnail!))!
            
            requestOperation.responseSerializer = AFImageResponseSerializer()
            requestOperation.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                print(responseObject)
                
                (cell as! BDHighlightCell).cell_Thumbnail.image = responseObject as? UIImage
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            requestOperation.start()
        }
        else {
            selectedIndexPaths.addObject(indexPath)
            let requestOperation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _Highlight[indexPath.section].matchs![indexPath.row].thumbnail!))!
            
            requestOperation.responseSerializer = AFImageResponseSerializer()
            requestOperation.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                print(responseObject)
                
                (cell as! BDHighlightCell).cell_Thumbnail.image = responseObject as? UIImage
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            requestOperation.start()
        }
    }
    
//MARK: PLAY MP4
    func playVideo(url:String) {
        
        let player = AVPlayer(URL: NSURL(string: url)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.presentViewController(playerController, animated: true) {
            player.play()
        }
    }
    
    func parseDataVideo(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "https://xemthethao.tv/wp-admin/admin-ajax.php?action=highlight_detail_mb&id=\(id!)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    if let dataSource = dataReponse["source"] as? String {
                            self.playVideo(dataSource)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func parseDataShareFB(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_SHAREFB)&accessToken=\(accessToken)&fid=\(fid)"
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let _data = responseObject["data"] as? NSDictionary {
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    @IBAction func btnShare(sender: UIBarButtonItem) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            parseDataShareFB()
            Utility.shareFB(self)
            
        }else {
            Utility.showAlert("Bạn chưa đăng nhập, vui lòng đăng nhập")
        }
    }
    
    
    
}

//MARK: ScrollView
extension BDHighlight:UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        print(isLoad)
        if (self.tableHighlight.contentSize.height - self.tableHighlight.contentOffset.y) <= 500 && isLoad == true {
            isLoad = false
            
            page += 1
            parseData()
        }
    }
}

//MARK: TableViewDelegate
extension BDHighlight:UITableViewDelegate {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return _Highlight[section].teamGroup
    }
}


//MARK: TableViewDataSource
extension BDHighlight:UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return _Highlight.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _Highlight[section].matchs!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellHighlight", forIndexPath: indexPath) as! BDHighlightCell
        cell.cell_Thumbnail.image = UIImage(named: "BDLogo")
        
        let cellTitle:String? = _Highlight[indexPath.section].matchs![indexPath.row].teams!.name1!
            + " " + _Highlight[indexPath.section].matchs![indexPath.row].teams!.score1! + "-" + _Highlight[indexPath.section].matchs![indexPath.row].teams!.score2! + " " + _Highlight[indexPath.section].matchs![indexPath.row].teams!.name2!
        
        cell.cell_Title.text = cellTitle
        cell.cell_League.text = _Highlight[indexPath.section].matchs![indexPath.row].league
        
//        cell.selectionStyle = .None
        configure(cell, forRowAtIndexPath: indexPath)

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        id = _Highlight[indexPath.section].matchs![indexPath.row].idMatchHigh
        parseDataVideo()
        
    }
    
}
