//
//  BDWatchOnline.swift
//  BongDa
//
//  Created by APPLE on 4/17/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FBSDKShareKit


class BDWatchOnline:UIViewController, FBSDKSharingDelegate{
    @IBOutlet weak var tableWatchOnline:UITableView!
    var _WatchOnline = [WatchOnline]()
    
    //INFO GET MATCH
    var idMatch:Int?
    var verify:String = "true"
    var fid:String!
    var accessToken:String!
    var refreshTable:UIRefreshControl!
    override func viewDidLoad() {
        
        
        
        setupView()
        setupData()
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
        }
    }
    
    func setupView(){
        
        Utility.setTitleNavi(self)
    
        refreshTable = UIRefreshControl()
        refreshTable.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshTable.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableWatchOnline.addSubview(refreshTable)
        
        tableWatchOnline.delegate = self
        tableWatchOnline.dataSource = self
        let gesture = UITapGestureRecognizer(target: self, action: "showZopimAction:")
        let fab = KCFloatingActionButton()
        self.view.addSubview(fab)
        fab.addGestureRecognizer(gesture)

        
        
    }
    
    func showZopimAction(sender:UITapGestureRecognizer){
        // do other task
        self.performSegueWithIdentifier("viewZopim", sender: nil)
    }
    
    func setupData(){
        parseData()
        
    }
    
    
    func parseData(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = URL_WATCHONLINE
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let dataReponse = responseObject["data"] as? [NSDictionary] {
                    for dicData in dataReponse {
                        self._WatchOnline.append(WatchOnline(dic_WatchOnline: dicData))
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.tableWatchOnline.reloadData()
                            if self.refreshTable.refreshing{
                                self.refreshTable.endRefreshing()
                                self.refreshTable = nil
                            }
                        })
                    }
                }else {
                    if self.refreshTable.refreshing{
                        self.refreshTable.endRefreshing()
                        self.refreshTable = nil
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    
    func parseDataGetLink(){
        
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_GETLINKWATCH)&id=\(idMatch!)&accessToken=\(accessToken)&fid=\(fid)&verify=\(verify)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                print(responseObject)
                if let _success = responseObject["success"] as? Int {
                    if _success == 0 {
                        Utility.showAlert(responseObject["data"]!["message"] as! String)
                    }else {
                        if let dataReponse = responseObject["data"] as? NSDictionary {
                            if let urlStream = dataReponse["stream"] as? String {
                                self.playVideo(urlStream)
                            }
                        }
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    
    func parseDataShareFB(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_SHAREFB)&accessToken=\(accessToken)&fid=\(fid)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let _data = responseObject["data"] as? NSDictionary {
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    
    func playVideo(url:String) {
        
        let player = AVPlayer(URL: NSURL(string: url)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.presentViewController(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func btnShare(sender: UIBarButtonItem) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            parseDataShareFB()
            Utility.shareFB(self)
            
        }else {
            Utility.showAlert("Bạn chưa đăng nhập, vui lòng đăng nhập")
        }
        
    }
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        
        print("share thanh cong")
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        print("error")
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        print("cancel")
    }
    
    func refresh(sender:AnyObject) {
        parseData()
    }
    
}

extension BDWatchOnline:UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 130
    }
}

extension BDWatchOnline:UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _WatchOnline.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellTitle", forIndexPath: indexPath)
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellData", forIndexPath: indexPath) as! CellData
            
            cell.cell_Logo_Team_1.image = _WatchOnline[indexPath.row-1].live_logo_a
            cell.cell_Logo_Team_2.image = _WatchOnline[indexPath.row-1].live_logo_b
            cell.cell_Title_Team_1.text = _WatchOnline[indexPath.row-1].team1
            cell.cell_Title_Team_2.text = _WatchOnline[indexPath.row-1].team2
            cell.cell_League.text = _WatchOnline[indexPath.row-1].league
            cell.cell_Date.text = _WatchOnline[indexPath.row-1].date
            cell.cell_Time.text = _WatchOnline[indexPath.row-1].time
            if _WatchOnline[indexPath.row-1].cost == false {
                cell.cell_Price.hidden = true
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            return;
        }
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
            idMatch = _WatchOnline[indexPath.row-1].id
            parseDataGetLink()
        }else {
            Utility.showAlert("Vui lòng đăng nhập để xem trực tuyến.")
        }
    }
}


