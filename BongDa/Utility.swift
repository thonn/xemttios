//
//  Utility.swift
//  BongDa
//
//  Created by APPLE on 5/3/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import Foundation
import FBSDKShareKit

class Utility {
    static func showAlert(Message: String){
        let alert = UIAlertView(title: Message, message: "", delegate: self, cancelButtonTitle: "OK")
        alert.show()
    }
    
    static func setTitleNavi(titleBar: UIViewController) {
        let logo = UIImage(named: "BDLogo")
        let imageView = UIImageView(image:logo)
        imageView.frame.size = CGSize(width: 30, height: 30)
        imageView.contentMode = .ScaleAspectFit
        titleBar.navigationItem.titleView = imageView
    }
    
    static func shareFB(viewShare: UIViewController){
        let content:FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = NSURL(string: "https://xemthethao.tv")
        let dialog:FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = viewShare
        dialog.shareContent = content
        dialog.mode = .Automatic
        dialog.show()
        
    }
}