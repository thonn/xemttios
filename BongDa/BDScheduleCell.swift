
//
//  BDScheduleCell.swift
//  BongDa
//
//  Created by APPLE on 4/18/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class BDScheduleCell:UITableViewCell {
    @IBOutlet weak var cell_Logo_1:UIImageView!
    @IBOutlet weak var cell_Logo_2:UIImageView!
    @IBOutlet weak var cell_Team_1:UILabel!
    @IBOutlet weak var cell_Team_2:UILabel!
    @IBOutlet weak var cell_Time:UILabel!
}
