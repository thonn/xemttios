//
//  Highlight.swift
//  BongDa
//
//  Created by APPLE on 4/19/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class Highlight {
    var teamGroup:String?
    var matchs:[MatchsHighlight]?
    init (dic_Highlight:NSDictionary){
        if let _teamGroup = dic_Highlight["teamGroup"] as? String {
            teamGroup = _teamGroup
        }
        if let _matchs = dic_Highlight["matchs"] as? [NSDictionary] {
            var arrMat:[MatchsHighlight] = []
            for mats in _matchs {
                arrMat.append(MatchsHighlight(dic_MatchsHighlight: mats))
            }
            matchs = arrMat
        }
    }
}


class MatchsHighlight {
    var idMatchHigh:Int?
    var league:String?
    var thumbnail:NSURL?
    var teams:TeamsHighlight?
    
    init (dic_MatchsHighlight:NSDictionary){
        if let _id = dic_MatchsHighlight["id"] as? Int {
            idMatchHigh = _id
        }
        if let _league = dic_MatchsHighlight["league"] as? String {
            league = _league
        }
        if let _thumbnail = dic_MatchsHighlight["thumbnail"] as? String {
//            let data = NSData(contentsOfURL: NSURL(string: _thumbnail)!)
            thumbnail = NSURL(string: _thumbnail)
        }
        if let _teams = dic_MatchsHighlight["teams"] as? NSDictionary {
            teams = TeamsHighlight(dic_TeamsHighlight: _teams)
        }
    }
}

class TeamsHighlight {
    var name1:String?
    var name2:String?
    var score1:String?
    var score2:String?
    init (dic_TeamsHighlight: NSDictionary){
        if let _name1 = dic_TeamsHighlight["name1"] as? String {
            print(_name1)
            
            name1 = _name1
        }
        if let _name2 = dic_TeamsHighlight["name2"] as? String {
            name2 = _name2
        }
        if let _score1 = dic_TeamsHighlight["score1"] as? String {
            score1 = _score1
        }
        if let _score2 = dic_TeamsHighlight["score2"] as? String {
            score2 = _score2
        }
    }
}