//
//  Account.swift
//  BongDa
//
//  Created by APPLE on 4/24/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class Account {
    var name:String?
    var coins:Int?
    var picture:NSURL?
    
    init(dic_Account:NSDictionary) {
        if let _coins = dic_Account["coins"] as? Int {
            coins = _coins
        }
        if let _name = dic_Account["name"] as? String{
            name = _name
        }
        if let _picture = dic_Account["picture"] as? String {
            picture = NSURL(string: _picture)!
        }
    }
}


class History {
    var action:String?
    var after:Int?
    var coins:String?
    var delta:String?
    var time:String?
    var type:String?
    
    init (dic_History: NSDictionary){
        if let _action = dic_History["action"] as? String {
            action = _action
        }
        if let _after = dic_History["after"] as? Int {
            after = _after
        }
        if let _coins = dic_History["coins"] as? String {
            coins = _coins
        }
        if let _delta = dic_History["delta"] as? String {
            delta = _delta
        }
        if let _time = dic_History["time"] as? String {
            time = _time
        }
        if let _type = dic_History["type"] as? String {
            type = _type
        }
    }
}