//
//  BDZopim.swift
//  BongDa
//
//  Created by ZALORA on 5/22/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class BDZopim: UIViewController {
    
    //MARK: Properties
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.hidden = true
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Quay lại", style: UIBarButtonItemStyle.Plain, target: self, action: "actionBack")
        // Do any additional setup after loading the view, typically from a nib.
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let messageView:UIWebView = UIWebView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height + (tabBarController?.tabBar.frame.height)!))
        messageView.loadRequest(NSURLRequest(URL: NSURL(string: "https://v2.zopim.com/widget/livechat.html?key=3v5kEDJLeeKbBrD6a0heJEW4Hxd9Sdg1&lang=en&hostname=xemthethao.tv&api_calls=%5B%5D")!))
        
        self.view.addSubview(messageView)
    }
    
    
    func actionBack(){
        tabBarController?.tabBar.hidden = false
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
