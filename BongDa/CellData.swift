//
//  CellData.swift
//  BongDa
//
//  Created by APPLE on 4/17/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class CellData:UITableViewCell {
    @IBOutlet weak var cell_Title_Team_1:UILabel!
    @IBOutlet weak var cell_Title_Team_2:UILabel!
    @IBOutlet weak var cell_Logo_Team_1:UIImageView!
    @IBOutlet weak var cell_Logo_Team_2:UIImageView!
    
    @IBOutlet weak var cell_View:UIView!
    @IBOutlet weak var cell_Price: UILabel!
    @IBOutlet weak var cell_League: UILabel!
    @IBOutlet weak var cell_Date: UILabel!
    @IBOutlet weak var cell_Time: UILabel!
    
    
    override func layoutSubviews() {

//        print("layoutSubviews")
//        self.layoutIfNeeded()
//        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        self.cell_View.layoutIfNeeded()
        self.cell_View.translatesAutoresizingMaskIntoConstraints = true
//        self.cell_Title_Team_1.layoutIfNeeded()
        self.cell_Title_Team_1.translatesAutoresizingMaskIntoConstraints = true
//        self.cell_Title_Team_2.layoutIfNeeded()
        self.cell_Title_Team_2.translatesAutoresizingMaskIntoConstraints = true
//        self.cell_Logo_Team_1.layoutIfNeeded()
        self.cell_Logo_Team_1.translatesAutoresizingMaskIntoConstraints = true
        self.cell_Logo_Team_2.translatesAutoresizingMaskIntoConstraints = true
        
//        self.yourButton.translatesAutoresizingMaskIntoConstraints = YES;
    }
    
 
}
