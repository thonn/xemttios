//
//  BDSchedule.swift
//  BongDa
//
//  Created by APPLE on 4/18/16.
//  Copyright © 2016 Camels. All rights reserved.
//

import UIKit

class BDSchedule:UIViewController {
    @IBOutlet weak var tableSchedule:UITableView!
    
    
    @IBOutlet weak var myTitle: UILabel!
    
    
    var _schedule = [Schedule]()
    var page:Int = 0
    var rowShow = 0
    var isLoad = true
    var accessToken:String!
    var fid:String!
//    var isFirst = true
    var selectedIndexPaths = NSMutableSet()
    override func viewDidLoad() {
        
        setupView()
        setupData()
    }
    
    override func viewDidAppear(animated: Bool) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            accessToken = NSUserDefaults.standardUserDefaults().objectForKey("access") as! String
            fid = NSUserDefaults.standardUserDefaults().objectForKey("fbid") as! String
        }
    }
    
    func setupView(){
        Utility.setTitleNavi(self)
        myTitle.text = "LỊCH THI ĐẤU"
        let gesture = UITapGestureRecognizer(target: self, action: "showZopimAction:")
        let fab = KCFloatingActionButton()
        self.view.addSubview(fab)
        fab.addGestureRecognizer(gesture)
        
        
        
    }
    
    func showZopimAction(sender:UITapGestureRecognizer){
        // do other task
        self.performSegueWithIdentifier("viewZopim", sender: nil)
    }
    
    func setupData(){
        parseData()
        tableSchedule.delegate = self
        tableSchedule.dataSource = self
    }
    
    
    func parseData(){
//        print(page)
        
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_SCHEDULE)\(page)"
        
        manager.GET(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
//                print(responseObject)
                
                if let dataReponse = responseObject["data"] as? NSDictionary {
                    
                    self._schedule.append(Schedule(dic_Schedule: dataReponse))
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.tableSchedule.reloadData()
                        
                        
                        
//                        let indexRow:Int = self._schedule[(self._schedule.count-1)].matchs!.count-1
//                        self.tableSchedule.reloadRowsAtIndexPaths([NSIndexPath(forRow: indexRow, inSection: self._schedule.count-1)], withRowAnimation: .Fade)
                        
                        
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.isLoad = true
                            if self.view.frame.size.height - self.tableSchedule.contentSize.height > 44 {
                                self.page += self.page + 1
                                self.parseData()
                            }
//                        })
                        
                    })
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }
    
    func configure(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if selectedIndexPaths.containsObject(indexPath) {
            let requestOperation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _schedule[indexPath.section].matchs![indexPath.row].teams!.logo1!))!
            
            requestOperation.responseSerializer = AFImageResponseSerializer()
            requestOperation.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                print(responseObject)
                
                (cell as! BDScheduleCell).cell_Logo_1.image = responseObject as? UIImage
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            
            
            let requestOperation2:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _schedule[indexPath.section].matchs![indexPath.row].teams!.logo2!))!
            
            requestOperation2.responseSerializer = AFImageResponseSerializer()
            requestOperation2.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                //                print(responseObject)
                
                (cell as! BDScheduleCell).cell_Logo_2.image = responseObject as? UIImage
                
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            requestOperation.start()
            requestOperation2.start()

        }else {
            selectedIndexPaths.addObject(indexPath)
            let requestOperation:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _schedule[indexPath.section].matchs![indexPath.row].teams!.logo1!))!
            
            requestOperation.responseSerializer = AFImageResponseSerializer()
            requestOperation.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                print(responseObject)
                
                (cell as! BDScheduleCell).cell_Logo_1.image = responseObject as? UIImage
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            
            
            let requestOperation2:AFHTTPRequestOperation = AFHTTPRequestOperation(request: NSURLRequest(URL: _schedule[indexPath.section].matchs![indexPath.row].teams!.logo2!))!
            
            requestOperation2.responseSerializer = AFImageResponseSerializer()
            requestOperation2.setCompletionBlockWithSuccess({ (operation, responseObject) -> Void in
                //                print(responseObject)
                
                (cell as! BDScheduleCell).cell_Logo_2.image = responseObject as? UIImage
                
                
                }) { (operation, error) -> Void in
                    print(error)
            }
            requestOperation.start()
            requestOperation2.start()
        }
    }
    
    @IBAction func btnShare(sender: UIBarButtonItem) {
        if (NSUserDefaults.standardUserDefaults().objectForKey("access")) != nil {
            parseDataShareFB()
            Utility.shareFB(self)
            
        }else {
            Utility.showAlert("Bạn chưa đăng nhập, vui lòng đăng nhập")
        }
    }
    
    func parseDataShareFB(){
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.requestSerializer.timeoutInterval = 20
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let URLString:String = "\(URL_SHAREFB)&accessToken=\(accessToken)&fid=\(fid)"
        
        manager.POST(URLString, parameters: [], success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject!) -> Void in
            if let responseObject = responseObject{
                if let _data = responseObject["data"] as? NSDictionary {
                    if let _message = _data["message"] as? String {
                        Utility.showAlert(_message)
                    }
                }
            }
            }) { (operation:AFHTTPRequestOperation!, error:ErrorType!) -> Void in
                print("Ket noi den he thong that bai")
        }
    }

}

extension BDSchedule:UIScrollViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        print(isLoad)
        if (self.tableSchedule.contentSize.height - self.tableSchedule.contentOffset.y) <= 500 && isLoad == true {
            isLoad = false
            
            page += 1
            parseData()
        }
    }
    
    
}

extension BDSchedule:UITableViewDelegate {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return _schedule[section].teamGroup
    }
}

extension BDSchedule:UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return _schedule.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (_schedule[section].matchs!.count)

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellSchedule", forIndexPath: indexPath) as! BDScheduleCell
        
        cell.cell_Logo_1.image = UIImage(named: "BDLogo")
        cell.cell_Logo_2.image = UIImage(named: "BDLogo")
        configure(cell, forRowAtIndexPath: indexPath)
        cell.cell_Team_1.text = _schedule[indexPath.section].matchs![indexPath.row].teams?.name1
        cell.cell_Team_2.text = _schedule[indexPath.section].matchs![indexPath.row].teams?.name2
        cell.cell_Time.text =  _schedule[indexPath.section].matchs![indexPath.row].time

//        self.tableSchedule.insertRowsAtIndexPaths([NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.section)], withRowAnimation: .Fade)
        
        

        return cell
    }

    
}